/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import java.io.IOException;
/**
 *
 * @author Cristian
 */
public class ArbolTrie{
    
    NodoTrie raiz;

    public ArbolTrie() {
        this.raiz = new NodoTrie("0");
    }
    
    public boolean agregar(String dato){
        if(dato==null)
            return false;
        dato = quitarTildes(dato);
        if(!comprobar(dato))
            return false;
        if(buscar(dato))
            return false;
        else{
              String dat[] = dato.split("");
              agregar(0, dat, this.raiz);
              return true; 
            }
    }
    
   private void agregar(int index, String dat[], NodoTrie letra){//{l,l,a,v,e}
        
            if(index == dat.length){
                return;
            }
            ListaS<NodoTrie> hijosLetra = letra.getHijos();   
            String letraPalabra = dat[index];  //l
            
            if(index+1 < dat.length){
                if(esDoble(dat[index], dat[index+1])){
                    letraPalabra+=dat[index+1];  //ll
                    index++;
                }
            }
            NodoTrie letraBuscada = letraEsta(letraPalabra, hijosLetra);
            if(letraBuscada == null){
                
                NodoTrie nuevo = new NodoTrie(letraPalabra);
                letra.setHijos(agregarAlf(hijosLetra, nuevo));
                nuevo.setFinPalabra(index+1 == dat.length);
                agregar(++index, dat, nuevo);
            }else{
                if(index+1 == dat.length)
                    letraBuscada.setFinPalabra(true);
                agregar(++index, dat, letraBuscada);
            }
    }
   
   private ListaS<NodoTrie> agregarAlf(ListaS<NodoTrie> l, NodoTrie n){
       char insertar = n.getInfo().charAt(0);
       ListaS<NodoTrie> aux = new ListaS<>();
       boolean inserto = false;
       if(l.esVacia()){
           l.insertarAlFinal(n);
           return l;
       }
            for(NodoTrie no : l){
                if((n.getInfo().length() == 1) && (no.getInfo().charAt(0)-insertar == 0)){
                    aux.insertarAlFinal(n);
                    inserto=true;
                }
                if(!inserto && !(no.getInfo().charAt(0)-insertar <= 0)){
                         aux.insertarAlFinal(n);
                         inserto=true;
                }
                aux.insertarAlFinal(no);
            }
            if(!inserto)
                aux.insertarAlFinal(n);
       return aux;
   }
    
    private boolean esDoble(String letra1, String letra2){
        return (letra1.equals("c") && letra2.equals("h"))
                || (letra1.equals("l") && letra2.equals("l"))
                || (letra1.equals("r") && letra2.equals("r"));
    }
    
    private String quitarTildes(String datos){
        datos = datos.toLowerCase();
        String rta = "";
        char d[] = datos.toCharArray();
        for (int i = 0; i < d.length; i++) {
            if(d[i] == 'á' || d[i] == 'à')
                d[i] = 'a';
            if(d[i] == 'é' || d[i] == 'è')
                d[i] = 'e';
            if(d[i] == 'í' || d[i] == 'ì')
                d[i] = 'i';
            if(d[i] == 'ó' || d[i] == 'ò')
                d[i] = 'o';
            if(d[i] == 'ú' || d[i] == 'ù')
                d[i] = 'u';
            rta+=d[i]; 
        }
        return rta;
    }
    
    public boolean buscar(String palabra){
        boolean existe = buscar(palabra, this.raiz, 0, false);
        if(existe)
            buscar(palabra, this.raiz, 0, true);
        return existe;
    }
    
    private boolean buscar(String palabra, NodoTrie l,int index, boolean existe){
        
        if(index != palabra.length()){
            String c = palabra.charAt(index)+"";
                if(index+1 < palabra.length()){
                        if(esDoble(palabra.charAt(index)+"", palabra.charAt(index+1)+"")){
                            c+=palabra.charAt(index+1); 
                            index++;
                        }
                    }
            ListaS<NodoTrie> hijosLetra = l.getHijos();
            NodoTrie aux = letraEsta(c, hijosLetra);
            
                if(aux != null){
                    if(existe)
                        aux.setSeBusca(true);
                    if(index == palabra.length()-1)
                        return true;
                    return buscar(palabra, aux, ++index,existe);
                }
        }
        return false;
    }

    public void crearPDF(String palabra){
        try{
            PdfWriter writer = palabra==null ? new PdfWriter("ArbolTrie.pdf") : new PdfWriter("busqueda.pdf");
            PdfDocument doc = new PdfDocument(writer);
            PageSize ps = PageSize.A4;
            PdfPage pp = doc.addNewPage(ps);
            PdfCanvas canva = new PdfCanvas(pp);
            PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA);
            canva.setStrokeColor(new DeviceRgb(23, 32, 42))
                            .fillStroke().beginText().setFontAndSize(font, 10).setColor(new DeviceRgb(23, 32, 42), true)//color letra
                            .fillStroke().setTextMatrix(20,815).showText("Cristian Medina - Johan Leon - Arbol Trie, Analisis de Algoritmos - Ing. Marco Adarme.")
                            .endText().stroke();
            if(palabra != null){
                canva.beginText().setFontAndSize(font, 10).setColor(new DeviceRgb(23, 32, 42), true)//color letra
                            .fillStroke().setTextMatrix(250,800-(getProfundidad()*50)).showText("Se buscó \""+palabra+"\"")
                            .endText().stroke();
            }
                
            float x =  ps.getWidth();
            float y = ps.getHeight()-50;
            float radio = 10;
            float distancia = x;
            trazar(canva,font,distancia,radio,x,y);
            doc.close();
        }catch(IOException e){
            System.err.print(e.getMessage());
        }
        
    }
    
    private void trazar(PdfCanvas canva, PdfFont font, float distancia, float radio, float x, float y){
        float i,xAux,yAux;
        int nivel = 1;
        float distAux = 0;
        i=distancia/2;
        distAux = x/this.getNodosNivel(nivel);
        xAux = distAux/2;
        yAux = y-(radio*4);
        Cola<NodoTrie> c = new Cola();
        c.enColar(this.raiz);
        Cola<NodoTrie> aux = new Cola();  //cola para los hijos de la raiz
            while(!c.esVacia()){
                NodoTrie a = c.deColar(); //raiz
                ListaS<NodoTrie> l = a.getHijos();   //hijos de la raiz
                
                crearCirculo(canva, font, i, y, radio, a);
                
                for (NodoTrie n : l) { //itero los hijos del nodo
                    canva.moveTo(i, (y-radio*2));
                    canva.lineTo(xAux, yAux);
                    canva.stroke();
                    xAux+=distAux;
                    aux.enColar(n);
                }
                i+=distancia;
                if(c.esVacia()){
                    c = aux; //paso el nuevo nivel acumulado en la cola aux a c
                    aux = new Cola<>(); //aux vuelve a null solo si ya se vacio un nivel de c
                    nivel++;
                    distancia = x/c.getTamanio()+1;
                    i = distancia/2;
                    y= y-(radio*4);
                    distAux = x/(getNodosNivel(nivel));
                    xAux = distAux/2;
                    yAux = y-(radio*4)-5;
                    
                }
            }
    
    }
    
    private void crearCirculo(PdfCanvas canva, PdfFont font, float i, float y, float radio, NodoTrie a){
        
        if(a.isFinPalabra() && !a.isSeBusca()){
                    canva.circle(i, y-radio, radio).setColor(new DeviceRgb(164, 234, 93), true);
                            }
        else if(a.isSeBusca()){
           a.setSeBusca(false);
           canva.circle(i, y-radio, radio).setColor(new DeviceRgb(101, 181, 222), true); 
        }
        else{
                    canva.circle(i, y-radio, radio).setColor(new DeviceRgb(255, 255, 255), true);}//color circulo//color circulo
                            canva.setStrokeColor(new DeviceRgb(23, 32, 42))
                            .fillStroke()
                            .beginText()
                            .setFontAndSize(font, 10)
                            .setColor(new DeviceRgb(23, 32, 42), true)//color letra
                            .fillStroke()
                            .setTextMatrix(i-3, y-radio)
                            .showText(a.getInfo())
                            .endText()
                            .stroke();
    }
    
    private int getNodosNivel(int nivel){
        
        if(nivel == 0)
            return 1;
        Cola<NodoTrie> c = new Cola();
        c.enColar(this.raiz);
        int anchura = 0;
        Cola<NodoTrie> aux = new Cola();  //cola para los hijos de la raiz
        int nodos = 0;
        while(!c.esVacia()){
            
            NodoTrie a = c.deColar(); //raiz
            ListaS<NodoTrie> l = a.getHijos();   //hijos de la raiz
               
            for (NodoTrie n : l) { //itero los hijos del nodo
                aux.enColar(n);
            }
            if(c.esVacia()){
                if(aux.getTamanio() > anchura){
                    anchura = aux.getTamanio();
                }
                c = aux; //paso el nuevo nivel acumulado en la cola aux a c
                nivel--;
                if(nivel <= 0){
                    nodos = c.getTamanio();
                    break;
                }
                aux = new Cola<>(); //aux vuelve a null solo si ya se vacio un nivel de c    
            }
        }
        return nodos;
    }
    
    public int getProfundidad(){
        return getProfundidad(this.raiz, 0, new int[1]);
    }
    
    private int getProfundidad(NodoTrie n, int cont, int []mayor){
    
        if(n.getHijos().esVacia()){
            if(cont>mayor[0])
                mayor[0] = cont;
            cont = 0;
            return cont;
        }
        ListaS<NodoTrie> l = n.getHijos();
        int i = 0;
        for (NodoTrie no : l) {
            if(i>0)
                cont -= 1;  //le resto uno cada vez que salte a otra rama
              getProfundidad(no, ++cont,mayor);
            i++;
        }
        return mayor[0];
    }
    
    private int getAnchura(){
        
        Cola<NodoTrie> c = new Cola();
        c.enColar(this.raiz);
        int anchura = 0;
        Cola<NodoTrie> aux = new Cola();  //cola para los hijos de la raiz
        while(!c.esVacia()){
            NodoTrie a = c.deColar(); //raiz
            ListaS<NodoTrie> l = a.getHijos();   //hijos de la raiz
               
            for (NodoTrie n : l) { //itero los hijos del nodo
                
                aux.enColar(n);
            }
            if(c.esVacia()){
                if(aux.getTamanio() > anchura){
                    anchura = aux.getTamanio();
                }
                c = aux; //paso el nuevo nivel acumulado en la cola aux a c
                aux = new Cola<>(); //aux vuelve a null solo si ya se vacio un nivel de c
            }
        }
        return anchura;
    }
    
    @Override
    public String toString(){
        String s[] = {""};
        return toString(this.raiz.getHijos(),"",s);
    }
    
    private String toString(ListaS<NodoTrie> l, String rta, String[] s){
    
        if(l.esVacia())
            return "";
        for(NodoTrie dato : l){
            
            if(!dato.equals(l.get(0))){ //para que no guarde el primero 
               
                char aux[] = rta.toCharArray();//ho
                rta="";
                for (int i = 0; i < aux.length-1; i++) {
                    rta+=aux[i];
                }
                rta+=dato.getInfo();
            }else
                rta +=dato.getInfo();
            
            if(dato.isFinPalabra()){
                s[0] += rta+";";
            }
            toString(dato.getHijos(),rta,s);
        } 
        return s[0];
    }
    
    private NodoTrie letraEsta(String let, ListaS<NodoTrie> l){
        
        if(l.esVacia())
            return null;
        for (NodoTrie letra : l) {
            if(letra.getInfo().equals(let))
                return letra;
        }
        return null;
    }
    
    private boolean comprobar(String dato){
        
        if(dato == null)
            return false;
        char arr[] = dato.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] < 97){
                return false;
            }
            if(arr[i] > 122){
                if(arr[i] != 'ñ'){
                        return false;
                    }
            }
        }
        return true;
    }

    public NodoTrie getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoTrie raiz) {
        this.raiz = raiz;
    }
}
