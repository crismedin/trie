/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author Cristian
 */
public class NodoTrie {
    
    private String info;
    private ListaS<NodoTrie> hijos;
    private boolean seBusca;
    private boolean finPalabra;

    public NodoTrie(String info) {
        this.info = info;
        this.hijos = new ListaS();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ListaS<NodoTrie> getHijos() {
        return hijos;
    }

    public void setHijos(ListaS<NodoTrie> hijos) {
        this.hijos = hijos;
    }

    public boolean isFinPalabra() {
        return finPalabra;
    }

    public void setFinPalabra(boolean finPalabra) {
        this.finPalabra = finPalabra;
    }

    public boolean isSeBusca() {
        return seBusca;
    }

    public void setSeBusca(boolean seBusca) {
        this.seBusca = seBusca;
    }

    
}
